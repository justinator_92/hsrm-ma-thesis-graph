#include <tensorflow/c/c_api.h>

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <vector>
#include <iostream>


using namespace std;


TF_Buffer * read_file(const char* file);
void read_graph(TF_Graph* graph);

void free_buffer(void* data, size_t length) {
    free(data);
}

static void Deallocator(void* data, size_t length, void* arg) {
    free(data);
}

int main() {
    cout << "Hello from TensorFlow Version: " << TF_Version() << endl;
    TF_Buffer* graph_def = read_file("graph.pb");

    TF_Graph* graph = TF_NewGraph();
    TF_Status* status = TF_NewStatus();

    TF_ImportGraphDefOptions* graph_opts = TF_NewImportGraphDefOptions();
    TF_GraphImportGraphDef(graph, graph_def, graph_opts, status);

    if (TF_GetCode(status) != TF_OK) {
        cout << "ERROR: Unable to import graph" << TF_Message(status) << endl;
        return -1;
    }
    read_graph(graph);

    /***
     * Create Input Variables
     */
    float values_a[4] = {1.0, 2.0, 3.0, 4.0};
    float values_b[4] = {2.0, 3.0, 4.0, 5.0};

    int64_t dims[] = {2, 2};
    int64_t out_dims[] = {2, 2};

    TF_Output input_operation[2];
    TF_Tensor *input_tensors[2];

    TF_Operation* a = TF_GraphOperationByName(graph, "a");
    TF_Operation* b = TF_GraphOperationByName(graph, "b");
    if (a == NULL || b == NULL) {
        std::cout << "Input Tensor is NULL" << std::endl;
        return -1;
    }

    TF_Output a_out = {a, 0};
    TF_Output b_out = {b, 0};
    input_operation[0] = a_out;
    input_operation[1] = b_out;

    TF_Tensor* a_input = TF_NewTensor(TF_FLOAT, dims, 2, &values_a, 4 * sizeof(float), &Deallocator, 0);
    TF_Tensor* b_input = TF_NewTensor(TF_FLOAT, dims, 2, &values_b, 4 * sizeof(float), &Deallocator, 0);
    input_tensors[0] = a_input;
    input_tensors[1] = b_input;

    /***
     * Create Output Variables
     */
    TF_Output output_operations[1];
    TF_Tensor *output_tensors[1];

    TF_Operation* mat_mul = TF_GraphOperationByName(graph, "mult");
//    cout << "input op info: " << TF_OperationNumOutputs(mat_mul) << endl;
    output_operations[0] = TF_Output{mat_mul, 0};  // TF_Output: struct{TF_Operation* operation, int index}

    TF_Tensor* output_value = TF_AllocateTensor(TF_FLOAT, out_dims, 2, 4 * sizeof(float));
    output_tensors[0] = output_value;


    // ###############################
    //           RUN SESSION
    // ###############################

    TF_SessionOptions* options = TF_NewSessionOptions();
    TF_Session* session = TF_NewSession(graph, options, status);

    assert(TF_GetCode(status) == TF_OK);
    assert(graph != NULL);

    std::cout << "Start session... " << std::endl;

    // Call TF_SessionRun
    TF_SessionRun(session, NULL,
                  &input_operation[0], &input_tensors[0], 2,
                  &output_operations[0], &output_tensors[0], 1,
                  NULL, 0, NULL, status);

    float* out_vals = static_cast<float*>(TF_TensorData(output_tensors[0]));
    while(*out_vals)
    {
        std::cout << "Output values info: " << *out_vals++ << "\n";
    }

    std::cout << "Session finished... clean up" << std::endl;

    TF_CloseSession(session, status);

    TF_DeleteSession(session, status);

    // Clean up...
    TF_DeleteImportGraphDefOptions(graph_opts);
    TF_DeleteStatus(status);
    TF_DeleteBuffer(graph_def);

    TF_DeleteGraph(graph);
    return 0;
}

void read_graph(TF_Graph* graph) {
    size_t pos = 0;
    TF_Operation* operation;

    while ((operation = TF_GraphNextOperation(graph, &pos)) != NULL) {
        std::cout << "Operation Name in graph: " << TF_OperationName(operation) << std::endl;
    }
}

TF_Buffer* read_file(const char* file) {
    FILE *f = fopen(file, "rb");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  //same as rewind(f);

    void* data = malloc(fsize);
    fread(data, fsize, 1, f);
    fclose(f);

    TF_Buffer* buf = TF_NewBuffer();
    buf->data = data;
    buf->length = fsize;
    buf->data_deallocator = free_buffer;
    return buf;
}
