import tensorflow as tf
import numpy as np

with tf.Session() as sess:
    a = tf.Variable([[5.0, 4.0], [5.0, 3.0]], name='a')
    b = tf.Variable([[6.0, 2.0], [2.0, 1.0]], name='b')
    c = tf.multiply(a, b, name='mult')

    sess.run(tf.global_variables_initializer())

    print(a.eval())
    print(b.eval())
    print(c.eval())

    tf.train.write_graph(sess.graph_def, './', 'graph.pb', as_text=False)

