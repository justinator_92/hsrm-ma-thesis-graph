#include "../include/graph.h"

#include <assert.h>
#include <stdlib.h>

#include <iostream>
#include <fstream>
#include <sstream>

#include <chrono>


using namespace std;


static void Deallocator(void* data, size_t length, void* arg) {
    free(data);
}

void free_buffer(void* data, size_t length) {
    free(data);
}

/**
 * Constructor for Graph Class. Performing Image Segmenation using a frozen TensorFlow Graph
 * @param file_name name of the frozen graph
 * @param batch_size size of batches to feed into the Neural Network
 * @param image_width with of the given images
 * @param image_height height of the given images
 * @param channels number of channels of input images
 * @param classes number of object classes used for segmentation
 */
Graph::Graph(string file_name, int batch_size, int image_width, int image_height, int channels, int classes) {
    cout << "Using TensorFlow Version: " << TF_Version() << endl;

    graph = TF_NewGraph();
    status = TF_NewStatus();
    graph_def = read_graph_file(file_name);
    graph_opts = TF_NewImportGraphDefOptions();

    TF_GraphImportGraphDef(graph, graph_def, graph_opts, status);
    assert(TF_GetCode(status) == TF_OK);

    this->batch_size = batch_size;
    this->image_width = image_width;
    this->image_height = image_height;
    this->channels = channels;
    this->classes = classes;

    cout << "Create Session Objects" << endl;
    options = TF_NewSessionOptions();
    session = TF_NewSession(graph, options, status);
}

/**
 * Method reads out all operations from the frozen TensorFlow Graph
 */
void Graph::read_graph() {
    size_t pos = 0;
    TF_Operation* operation;

    while ((operation = TF_GraphNextOperation(graph, &pos)) != NULL) {
        cout << "Operation Name: " << TF_OperationName(operation) << endl;
    }
}

/**
 * Create all Operations and Tensors for Segmentation
 * @param image_data the raw image data for an image to be segmented
 */
void Graph::createGraph(float * image_data) {
    int64_t input_dimensions[] = {batch_size, image_width, image_height, channels};
    float keep_probabilities[] = {1.0};
    int64_t one_dimensional[] = {1};

    // Create Input Tensors
    TF_Operation* input_batch = TF_GraphOperationByName(graph, "ALEX/input");
    TF_Operation* input_probability = TF_GraphOperationByName(graph, "ALEX/input_keep_probability");

    assert(input_batch != NULL && input_probability != NULL);

    //  The name 'Model/probabilities' refers to an Operation, not a Tensor.
    // Tensor names must be of the form "<op_name>:<output_index>".
    input_operations[0] = TF_Output{input_batch, 0};
    input_operations[1] = TF_Output{input_probability, 0};

    TF_Tensor* input_batch_tensor = TF_AllocateTensor(TF_FLOAT, input_dimensions, 4,
                                                      batch_size * image_width * image_height * channels * sizeof(float));

    TF_Tensor* input_probability_tensor = TF_NewTensor(TF_FLOAT, one_dimensional, 1, &keep_probabilities, sizeof(float),
                                                       &Deallocator, 0);

    input_tensors[0] = input_batch_tensor;
    input_tensors[1] = input_probability_tensor;

    float *handler = (float*) TF_TensorData(input_tensors[0]);
    for (int i = 0; i < batch_size * image_width * image_height * channels; i += 1) {
        handler[i] = image_data[i];
    }

    assert(input_batch_tensor != NULL && input_probability_tensor != NULL);

    // Create Output Tensor
    int64_t output_dimensions[] = {batch_size, image_width, image_height, classes};

    TF_Operation* probabilities = TF_GraphOperationByName(graph, "Model/probabilities");
    output_operations[0] = TF_Output{probabilities, 0};  // TF_Output: struct{TF_Operation* operation, int index}

    TF_Tensor* output_tensor = TF_AllocateTensor(TF_FLOAT, output_dimensions, 4,
                                                 batch_size * image_width * image_height * classes * sizeof(float));
    output_tensors[0] = output_tensor;

    // set flag to true when graph is fully initialized
    graph_initialized = true;
}

/**
 * Run the TensorFlow Session to perform image segmentation
 */
void Graph::runSession() {
    assert(graph_initialized);

    assert(TF_GetCode(status) == TF_OK);
    assert(graph != NULL);

    auto t_start = chrono::high_resolution_clock::now();

    // Run Session with Frozen Graph
    TF_SessionRun(session, NULL,
                  &input_operations[0], &input_tensors[0], 2,
                  &output_operations[0], &output_tensors[0], 1,
                  NULL, 0, NULL, status);

    auto t_end = chrono::high_resolution_clock::now();
    cout << "Run Session took " << chrono::duration<double, milli>(t_end - t_start).count() << "ms\n" << endl;

    // Copy image data from Output Tensor into output_batch container
    float *output_batch = (float *) malloc(batch_size * image_width * image_height * classes * sizeof(float));
    float *output_handler = (float *) TF_TensorData(output_tensors[0]);
    for (int i = 0; i < batch_size * image_width * image_height * classes; i += 1) {
        output_batch[i] = output_handler[i];
    }

    int* final_image = (int*) malloc(image_width * image_height * sizeof(int));

    // declare variable to determine maximum classes
    int max_index;
    float maximum, current;

    // Determine maximum of probability output map
    for (int i = 0; i < image_width * image_height; i += 1) {
        max_index = -1;
        maximum = -1.0;

        for(int c = 0; c < classes; c += 1) {
            current = output_batch[i * classes + c];
            if (current > maximum) {
                max_index = c;
                maximum = current;
            }
        }
        final_image[i] = max_index;
    }

    // write data to textfile... Later pass back to caller or write image using OpenCV...?
    ofstream myfile;
    myfile.open("output.txt");
    for (int i = 0; i < image_height * image_width; i += 1) {
        myfile << final_image[i] << "\n";
    }
    myfile.close();

    //TF_CloseSession(session, status);
    //TF_DeleteSession(session, status);
    //free(output_batch);
}

TF_Buffer * Graph::read_graph_file(string file) {
    FILE *f = fopen(file.c_str(), "rb");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  //same as rewind(f);

    void* data = malloc(fsize);
    fread(data, fsize, 1, f);
    fclose(f);

    TF_Buffer* buf = TF_NewBuffer();
    buf->data = data;
    buf->length = fsize;
    buf->data_deallocator = free_buffer;
    return buf;
}

/**
 * Constructor to free all allocated memory
 */
Graph::~Graph() {
    cout << "Destroy Graph..." << endl;
    TF_DeleteImportGraphDefOptions(graph_opts);
    TF_DeleteStatus(status);
    TF_DeleteBuffer(graph_def);
    TF_DeleteGraph(graph);

    // TF_DeleteTensor... on output tensor
}
