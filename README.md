Read a TensorFlow Graph ProtoBuffer Definition in C++ and Perform Forward Passes through the Network.

Install TensorFlow C-API as explained here:

https://www.tensorflow.org/install/install_c

Download a TensorFlow Graph Example (FCN-AlexNet for Intensity Images under:

https://www.dropbox.com/s/26glyrnozznl1zw/Graph_Freezed.pb?dl=0



