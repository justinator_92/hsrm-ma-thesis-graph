#include <tensorflow/c/c_api.h>

#include <vector>
#include <string>

#ifndef GRAPH_H
#define GRAPH_H

class Graph {

public:
    Graph(std::string file_name, int batch_size, int image_width, int image_height, int channels, int classes);
    void read_graph();
    void createGraph(float * image_data);
    void runSession();
    ~Graph();

private:
    int image_width;
    int image_height;
    int channels;
    int classes;
    int batch_size;
    bool graph_initialized = false;

    // Graph Pointers
    TF_Buffer* graph_def;
    TF_Graph* graph;
    TF_ImportGraphDefOptions* graph_opts;

    // Status Object
    TF_Status* status;

    // Input Ops
    TF_Output input_operations[2];
    TF_Tensor *input_tensors[2];

    // Output Ops
    TF_Output output_operations[1];
    TF_Tensor *output_tensors[1];

    TF_Buffer* read_graph_file(std::string file);

    TF_SessionOptions *options;
    TF_Session *session;
};

#endif //GRAPH_H
