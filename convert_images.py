import tensorflow as tf
import os
import skimage
import skimage.io
import skimage.color
import skimage.transform
import numpy as np
import cv2
import sys


CLASSES=4
s = 128

def read_image(path):
    img = cv2.imread(path, 0)
    img = skimage.transform.resize(img, (s, s), preserve_range=True).astype(np.uint8)
    #img = np.stack([img, img, img], axis=2)
    return img


def write_image_to_disk(image, file_name):
    print(image.shape)
    image = image.reshape(s * s)
    with open(file_name, 'a') as f:
        for v in image:
            f.write(str(v) + '\n')


def read_image_from_disk(image_file, png_file):
    print("convert file: " + image_file)
    image = []
    with open(image_file, 'r') as f:
        for line in f.readlines():
            image.append(float(line))

    image = np.array(image).reshape(s, s).astype(np.uint8) * 85    
    cv2.imwrite(png_file, image)


def classify_image(batch_images, output_name):
    with tf.Session() as session:
        output_graph_def = tf.GraphDef()
        with open("Graph_Freezed.pb", "rb") as f:
            output_graph_def.ParseFromString(f.read())
            session.graph.as_default()
            _ = tf.import_graph_def(output_graph_def, name="")

    output_node = session.graph.get_tensor_by_name("Model/probabilities:0")
    input_batch_images = session.graph.get_tensor_by_name("ALEX/input:0")
    inputKeepProbability = session.graph.get_tensor_by_name("ALEX/input_keep_probability:0")

    batch_images = batch_images.reshape(1, s, s, 1)

    batch_output = session.run(output_node, feed_dict={input_batch_images: batch_images,
                                                       inputKeepProbability: 1})

    img = convert_softmax_to_one_hot(batch_output[0])
    output = convert_one_hot_to_gray_image(img)
    cv2.imwrite(output_name, output)
    


def convert_softmax_to_one_hot(probability_map):
    assert (len(probability_map.shape) == 3)
    num_classes = probability_map.shape[2]

    depths = [probability_map[:, :, d] for d in range(num_classes)]  # separate channels
    probability_depths = []

    for depth_idx in range(num_classes):
        copy = list(depths)
        copy.pop(depth_idx)

        # stack boolean maps on each other...
        depths_for_level = np.stack([depths[depth_idx] > copy[idx] for idx in range(num_classes - 1)], axis=2)
        depth_map = np.all(depths_for_level, axis=2)
        probability_depths.append(depth_map.astype(np.uint8))

    return np.stack(probability_depths, axis=2)


def convert_one_hot_to_gray_image(probability_map):
    num_classes = probability_map.shape[2]

    depths = [probability_map[:, :, d] for d in range(num_classes)]  # split probability channels...
    gray_values = [int(g / (num_classes - 1) * 255) for g in range(num_classes)]
    probability_depths = []

    for depth_idx in range(num_classes):
        copy = list(depths)
        copy.pop(depth_idx)  # do not compare with same depth channel...

        # stack boolean maps on each other...
        depths_for_level = np.stack([depths[depth_idx] > copy[idx] for idx in range(num_classes - 1)], axis=2)
        depth_map = np.all(depths_for_level, axis=2)
        probability_depths.append(depth_map.astype(np.uint8) * gray_values[depth_idx])

    img = np.stack(probability_depths, axis=2)
    img = np.amax(img, axis=2)
    return img


if __name__ == '__main__':
    assert (len(sys.argv) >= 2)

    # convert png file to txt
    if sys.argv[1] == 'r':
        img_png_name = sys.argv[2]
        img_txt_name = sys.argv[3]

        if os.path.exists(img_txt_name):
            os.remove(img_txt_name)
        open(img_txt_name, 'a').close()  # create empty file

        write_image_to_disk(read_image(img_png_name), img_txt_name)

    # convert txt file to png
    if sys.argv[1] == 'w':
        txt_file_name = sys.argv[2]
        png_file_name = sys.argv[3]
        read_image_from_disk(txt_file_name, png_file_name)

    if sys.argv[1] == 'n':
        img = read_image(sys.argv[2])
        name = sys.argv[3]
        classify_image(img, name)
