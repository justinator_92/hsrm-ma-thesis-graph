CC = g++
CFLAGS = -I/usr/local/include -L/usr/local/lib -std=c++11 -pedantic -Wall -g

GOAL=graph

PY = python3

prog:
	$(CC) $(CFLAGS) lib/*.cpp main.cpp -ltensorflow -o $(GOAL)

clean:
	rm -rf $(GOAL) data.txt output.txt result.png

run:
	echo "Convert Test Image to Textfile"
	$(PY) convert_images.py r gray_000984.png data.txt
	
	echo "Run TensorFlow C++ Forward Pass"
	./$(GOAL)

	echo "Evaluate output"
	$(PY) convert_images.py w output.txt result.png

	echo "Clean up"
	rm -rf data.txt
	rm -rf output.txt

