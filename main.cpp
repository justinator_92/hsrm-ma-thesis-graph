#include "include/graph.h"
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>


using namespace std;

int HEIGHT = 128;
int WIDTH = 128;
int CHANNELS = 1;
int BATCH_SIZE = 1;
int CLASSES = 4;


float * read_image(const char* file_name) {
    float * values = (float *) malloc(BATCH_SIZE * HEIGHT * WIDTH * CHANNELS * sizeof(float));

    ifstream infile(file_name);
    string line;

    int counter = 0;
    float a;

    while (getline(infile, line)) {
        istringstream iss(line);
        if (!(iss >> a)) { break; }  // error

        values[counter++] = float(a);
    }
    cout << "read in " << counter << " values." << endl;
    return values;
}

int main() {
    float * data = read_image("data.txt");

    Graph g("Graph_Freezed.pb", BATCH_SIZE, HEIGHT, WIDTH, CHANNELS, CLASSES);
    //g.read_graph();

    g.createGraph(data);
    g.runSession();

    g.createGraph(data);
    g.runSession();

    return 0;
}
